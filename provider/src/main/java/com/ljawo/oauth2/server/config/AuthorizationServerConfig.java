package com.ljawo.oauth2.server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@EnableAuthorizationServer
@Configuration
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    // required to enable Resource Owner Password Credentials Grant
    @Autowired
    private AuthenticationManager authenticationManager;

    // required to enhance access tokens with some custom properties
    @Autowired
    private CustomTokenEnhancer customTokenEnhancer;

    // in memory user store - for testing
    @Autowired
    private InMemoryUserDetails inMemoryUserDetails;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(inMemoryUserDetails)
                .tokenEnhancer(customTokenEnhancer);
    }

    // to check a token you need to be authenticated (all authenticated clients are allowed to access the check_token endpoint):
    // curl  localhost:5000/oauth/check_token?token={ACCESS_TOKEN} -u client_1:secret_1
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.checkTokenAccess("isAuthenticated()");
    }

    // to obtain a new token:
    // 1. user to service flow:
    // curl localhost:5000/oauth/token -d "grant_type=password&scope=read&username=user_1&password=secret1" -u client_1:secret_1
    // 2. service to service flow:
    // curl localhost:5000/oauth/token -d "grant_type=client_credentials&scope=app1.read" -u client_2:secret_2
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                // a client with a Resource Owner Credentials Grant
                .withClient("client_1")
                .secret("secret_1")
                .authorizedGrantTypes("password", "refresh_token")
                .resourceIds("res-1", "res-2", "res-3")
                .autoApprove(true)
                .accessTokenValiditySeconds(3600)
                .refreshTokenValiditySeconds(24 * 3600)
                .and()
                // a client with a Client Credentials Grant
                .withClient("client_2")
                .secret("secret_2")
                .authorizedGrantTypes("client_credentials")
                .resourceIds("res-1", "res-4", "res-6") // client_2 is allowed to access these resources
                .accessTokenValiditySeconds(3600) // only access token, there is no refresh token
                .scopes("app1.read", "app2.write") // a set of available scopes, you can request a specific one
                .autoApprove(false)  //
                .and()
                // another client with a Client Credentials Grant
                .withClient("client_3")
                .secret("secret_3")
                .authorizedGrantTypes("client_credentials")
                .resourceIds("res-7", "res-8", "res-9")
                .scopes("s1", "s2", "s3")
                .accessTokenValiditySeconds(3600)
                .and()
                // a client with no grants, allowed to call the check_token_endpoint
                .withClient("check_token_client")
                .secret("secret")
                .authorizedGrantTypes();
    }
}