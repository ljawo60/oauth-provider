package com.ljawo.oauth2.server.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

// it's required to add some custom attributes
public class CustomUser extends User {

    private final Map<String, String> customAttributes;

    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Map<String, String> customAttributes) {
        super(username, password, authorities);
        this.customAttributes = customAttributes;
    }

    public Map<String, String> getCustomAttributes() {
        return Collections.unmodifiableMap(customAttributes);
    }

    @Override
    public void eraseCredentials() {
        // do nothing - it's a memory store
    }
}
