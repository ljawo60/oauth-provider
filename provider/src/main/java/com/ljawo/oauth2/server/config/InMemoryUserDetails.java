package com.ljawo.oauth2.server.config;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
public class InMemoryUserDetails implements UserDetailsManager {

    private Map<String, UserDetails> users = new HashMap<>();

    public InMemoryUserDetails() {
        Map<String, String> customAttributes1 = new HashMap<>();
        customAttributes1.put("unit_id", "123");
        customAttributes1.put("unit_name", "IT");
        CustomUser user1 = new CustomUser("user_1", "secret1", Arrays.asList(new SimpleGrantedAuthority("role_1"), new SimpleGrantedAuthority("role_2")), customAttributes1);


        Map<String, String> customAttributes2 = new HashMap<>();
        customAttributes2.put("unit_id", "456");
        customAttributes2.put("unit_name", "HR");
        CustomUser user2 = new CustomUser("user_2", "secret2", Arrays.asList(new SimpleGrantedAuthority("role_3"), new SimpleGrantedAuthority("role_4")), customAttributes2);
        users.put(user1.getUsername(), user1);
        users.put(user2.getUsername(), user2);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return users.get(username);
    }

    @Override
    public boolean userExists(String username) {
        return users.containsKey(username);
    }

    @Override
    public void createUser(UserDetails userDetails) {
        users.put(userDetails.getUsername(), userDetails);
    }

    @Override
    public void updateUser(UserDetails userDetails) {
        users.put(userDetails.getUsername(), userDetails);
    }

    @Override
    public void deleteUser(String username) {
        users.remove(username);
    }

    @Override
    public void changePassword(String username, String password) {
    }

}