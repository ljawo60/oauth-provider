# OAuth2 Provider

## Description

This project contains a sample OAuth2 Provider service and a sample webapp. 

## Clients

There are the following clients:

1. Client with a Resource Owner Credentials Grant; client_id: client_1, secret: secret_1, authorized grant types: password, refresh_token
2. Client with a Client Credentials Grant; client_id: client_2, secret: secret_2, authorized grant types: client_credentials
3. Another client with a Client Credentials Grant; client_id: client_3, secret: secret_3, authorized grant types: client_credentials
4. Client with no grants, allowed to call the check_token_endpoint; client_id: check_token_client, secret: secret, authorized grant types: N/A

## Users:

There are the following users:

1. User user1 with password: secret1 and privileges: role_1, role_2
1. User user2 with password: secret2 and privileges: role_3, role_4

## Obtaining an access token:

### Resource Owner Credentials Grant

To get an access token just provide client and user credentials and proper grant type, e.g:
```
curl localhost:5000/oauth/token -d "grant_type=password&scope=read&username=user_1&password=secret1" -u client_1:secret_1
```

You should receive a response like this:
```
{
  "access_token":"d884082a-c205-4e96-bdc3-a80f635044b7",
  "token_type":"bearer",
  "refresh_token":"89ef3c00-c9d7-4ee7-bf25-d90666e3f2f0",
  "expires_in":3599,
  "scope":"read",
  "unit_name":"IT",
  "unit_id":"123"
  "property_x":"value_x",
}
```

### Client Credentials Grant

To get an access token just provide client credentials and proper grant type, e.g:
```
curl localhost:5000/oauth/token -d "grant_type=client_credentials&scope=app1.read" -u client_2:secret_2
```

You should receive a response like this:
```
{
  "access_token":"8c965680-46e1-4211-b315-8af14640fd52",
  "token_type":"bearer",
  "expires_in":3599,
  "scope":"app1.read",
  "property_x":"value_x"
}
```

### Check token endpoint

```
curl  localhost:5000/oauth/check_token?token=d884082a-c205-4e96-bdc3-a80f635044b7 -u client_1:secret_1
```
Response:

```
{
  "unit_name":"IT",
  "aud":["res-3","res-1","res-2"],
  "property_x":"value_x",
  "user_name":"user_1",
  "scope":["read"],
  "exp":1484686581,
  "unit_id":"123",
  "authorities":["role_2","role_1"],
  "client_id":"client_1"
}
```
