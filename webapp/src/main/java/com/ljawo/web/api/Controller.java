package com.ljawo.web.api;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class Controller {

    // curl localhost:8080/api/info -H "Authorization: Bearer {ACCESS_TOKEN}"
    @GetMapping("/api/info")
    @PreAuthorize("hasAuthority('role_1') or #oauth2.hasScope('app1.read')")
    public Map info(@AuthenticationPrincipal OAuth2Authentication authentication) {

        Map<String, Object> info = new HashMap<>();

        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();

        info.put("principal", authentication.getPrincipal());
        info.put("authorities", authentication.getAuthorities());
        info.put("token", details.getTokenValue());
        info.put("details", details.getDecodedDetails());

        return info;
    }

}
